import QtQuick 2.15

import QtQuick.Window 2.15

//for layouts like grid
import QtQuick.Layouts

//For buttons
import QtQuick.Controls

Window {

    width: 1000
    height: 1000
    visible: true
    title: qsTr("Hello World")
    //Center Picture Box

    Rectangle {

        id: mainGui

        //Size of window
        width: Window.width
        height: Window.height

        anchors.bottom: centerPictureBox.top- height/4

        color: "purple"
        border.color: "black"
        border.width: 5
        radius: 10
    }


    Rectangle {
        id: centerPictureBox

        //Size of window
        width: Window.width/1.8
        height: Window.height/2

        //UpDown
        y: Window.height/2.2 - height/2
        //LeftRight
        x: Window.width/2 - width/2

        color: "red"
        border.color: "black"
        border.width: 5
        radius: 10

        Image {
            id: picture
            source: "qrc:/Images/frame_31_delay-0.09s.jpg"
            anchors.fill: parent
        }

    }

    Rectangle {

        id: question

        //Size of window
        //width: Window.width
        //height: 1

        anchors.right: mainGui.right
        anchors.left: mainGui.left
        anchors.top: mainGui.top
        anchors.bottom: centerPictureBox.top

        //UpDown
        y: centerPictureBox - height
        //LeftRight
        x: Window.width/2 - width/2

        color: "Blue"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: questionsText
            text: qsTr("How do you install Qt Charts")
            font.bold: true
            font.pointSize: 40
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {

        id: answerBox

        //Size of window
        width: Window.width
        height: Window.height/5
        anchors.bottom: mainGui.bottom
         anchors.top: centerPictureBox.bottom
        x: Window.width - width

        color: "Green"
        border.color: "black"
        border.width: 5
        radius: 10

        GridLayout
        {

            columns: 2
            anchors.fill: parent


            Button
            {
            text: "Look harder"
            font.bold: true
            font.pointSize: 20
            Layout.fillHeight: true
            Layout.fillWidth: true

            }
            Button
            {
            text: "It works for me"
            font.bold: true
            font.pointSize: 20
            Layout.fillHeight: true
            Layout.fillWidth: true

            }
            Button
            {
            text: "Help"
            font.bold: true
            font.pointSize: 20
            Layout.fillHeight: true
            Layout.fillWidth: true

            }
            Button
            {
            text: "You just have to install the lib"
            font.bold: true
            font.pointSize: 20
            Layout.fillHeight: true
            Layout.fillWidth: true

            }

        }
    }

}
